(use-modules (gnu))
(use-modules (gnu packages xorg))
(use-modules (gnu packages xfce))
(use-modules (gnu packages networking))
(use-modules (gnu services linux))
(use-service-modules desktop networking ssh xorg)
;; Import nonfree linux module.
(use-modules (nongnu packages linux)
             (nongnu system linux-initrd))
(use-modules (srfi srfi-1))
(use-modules (nongnu packages nvidia))
(use-modules (guix transformations))

(define transform
  (options->transformation
   '((with-graft . "mesa=nvda"))))

(operating-system
  (kernel linux-lts)
  (kernel-arguments
   (append
    '("modprobe.blacklist=nouveau")
    %default-kernel-arguments))
  (kernel-loadable-modules (list nvidia-module))
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (locale "en_GB.utf8")
  (timezone "Europe/Paris")
  (keyboard-layout (keyboard-layout "fr" "azerty"))
  (host-name "arya")
  (users (cons* (user-account
                 (name "jb")
                 (comment "jb")
                 (group "users")
                 (home-directory "/home/jb")
                 (supplementary-groups
                  '("wheel" "netdev" "audio" "video" "lp")))
                %base-user-accounts))
  (packages
   (append
    (list (specification->package "nss-certs"))
    %base-packages))
  (services
   (append
    (list (service xfce-desktop-service-type (xfce-desktop-configuration (xfce (transform xfce))))
          (service nvidia-service-type)
          (service slim-service-type (slim-configuration
                                      (display ":1")
                                      (vt "vt8")
                                      (xorg-configuration (xorg-configuration
                                                           (keyboard-layout keyboard-layout)
                                                           (modules (cons* nvidia-driver %default-xorg-modules))
                                                           (server (transform xorg-server))
                                                           (drivers '("nvidia")))))))
    (remove (lambda (service)
              (eq? (service-kind service) gdm-service-type))
            %desktop-services)))
  (bootloader
   (bootloader-configuration
    (bootloader grub-efi-bootloader)
    (target "/boot/efi")
    (keyboard-layout keyboard-layout)))
  (swap-devices (list "/dev/sda2"))
  (file-systems
   (cons* (file-system
            (mount-point "/boot/efi")
            (device (uuid "98CD-8BAC" 'fat32))
            (type "vfat"))
          (file-system
            (mount-point "/")
            (device
             (uuid "38e33184-b95d-4be0-b6c5-9fd0f565016a"
                   'ext4))
            (type "ext4"))
          %base-file-systems)))
